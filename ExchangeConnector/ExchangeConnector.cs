﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Exchange.WebServices.Data;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using ExchangeConnector.Data;
using ExchangeConnector.Config;
using NLog;
namespace ExchangeConnector
{
    public class ExchangeConnectorImplementation : ICalendarConnector
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static NameEmailMap NameEmailCache = new NameEmailMap();
        private static EmailAddressKey smtpKey = EmailAddressKey.EmailAddress1;
        private static EmailAddressKey[] smtpKeys = { EmailAddressKey.EmailAddress1, EmailAddressKey.EmailAddress2, EmailAddressKey.EmailAddress3 };
        private static bool isEmailAddressKeySet = false;
        private static object _lock = new Object();
        private static object _emailkeylock = new Object();
        private static bool useFixedSMTPkey = CalendarConfig.FixedSMTPIndex;
        private static LegacyFreeBusyStatus[] hideStatuses = CalendarConfig.hideStatuses;
        private static LegacyFreeBusyStatus[] busyStatuses = CalendarConfig.busyStatuses;
        public static ExchangeService GetExchangeService()
        {
            ExchangeService service = new ExchangeService(CalendarConfig.WsVersion);
            service.Credentials = GetCredentials();
            service.Url = new Uri(CalendarConfig.URL);

            return service;
        }
        private static WebCredentials GetCredentials()
        {
            return CalendarConfig.UseNetWorkCredentials ?
                new WebCredentials() : new WebCredentials(CalendarConfig.User
            , CalendarConfig.Pwd, CalendarConfig.Domain);
        }
        public UserAndAppointmentsCollection GetAppointmentsInPeriod(IEnumerable<string> emailaddresses, DateTime startTime, DateTime endTime)
        {
            var service = ExchangeConnectorImplementation.GetExchangeService();
            //..
            var userArray = emailaddresses.ToArray<string>();
            List<string> errormessages = new List<string>();
            if (userArray.Length > 100)
            {
                logger.Info("Exchange can only handle 100 recipients truncating array");
                Array.Resize(ref userArray, 100);
                errormessages.Add("You can only query 100 mailaddresses, showing the first 100");
            }
            logger.Log(LogLevel.Debug, "Resolving Names");
            var namearray = NamesFromEmail(emailaddresses);
            logger.Log(LogLevel.Debug, " Finished Resolving Names");
            logger.Log(LogLevel.Debug, "Calling GetUserAvailability");
            GetUserAvailabilityResults availability =
                       service.GetUserAvailability(namearray.Select(username => new AttendeeInfo(username.Email)),
                     new TimeWindow(startTime, endTime),
                     AvailabilityData.FreeBusy);
            logger.Log(LogLevel.Debug, String.Format("{0} - Exiting GetUserAvailability", availability.AttendeesAvailability.OverallResult));
            var appointments = new List<UserAndAppointments>();
            var i = 0;

            foreach (AttendeeAvailability attendeeAvailability in availability.AttendeesAvailability)
            {
                //var user = userArray[i++];
                var user = namearray.ElementAt(i++);
                if (attendeeAvailability.Result == ServiceResult.Success)
                {

                    var userapp = new UserAndAppointments();
                    var userappointments = new List<MyAppointment>();
                    foreach (CalendarEvent e in attendeeAvailability.CalendarEvents)
                    {
                        var myapp = new MyAppointment(e);

                        if (ShowEvent(e.FreeBusyStatus))
                        {
                            userapp.addAppointment(myapp);
                        }
                        if (myapp.Duration > 479)
                        {
                            userapp.addInfo(myapp);
                        }
                    }

                    userapp.Email = user.Email;
                    userapp.DisplayName = user.Name;
                    appointments.Add(userapp);
                }
                else
                {
                    var userapp = new UserAndAppointments(true, attendeeAvailability.ErrorMessage);
                    userapp.DisplayName = "##ERR##";
                    appointments.Add(userapp);
                    errormessages.Add(attendeeAvailability.ErrorMessage);
                }


            }

            return new UserAndAppointmentsCollection(appointments, startTime, endTime, errormessages);


        }

        private bool IsAllDay(CalendarEvent e)
        {
            throw new NotImplementedException();
        }
        private IEnumerable<NameEmail> NamesFromEmail(IEnumerable<string> emailaddresses)
        {
            List<NameEmail> Names = new List<NameEmail>(emailaddresses.Count());
            var service = ExchangeConnectorImplementation.GetExchangeService();
            foreach (var email in emailaddresses)
            {
                if (NameEmailCache.ContainsKey(email.ToLower()))
                {
                    Names.Add(NameEmailCache[email.ToLower()]);
                    logger.Trace("Cachehit {0}", email);
                    continue;
                }

                logger.Log(LogLevel.Debug, "Resolving Name - GetUser availiablity");
                var contact = service.ResolveName(email, ResolveNameSearchLocation.DirectoryOnly, true);
                TraceContact(contact);
                if (contact.Count == 1)
                {
                    var clnemail="";
                    //var clnemail= CleanEmailAddress(GetSmtpAddress(contact[0].Contact)).ToLower();
                    if (contact[0].Mailbox.MailboxType == MailboxType.PublicGroup)
                    {
                        ExpandGroupResults exp = service.ExpandGroup(contact[0].Mailbox.Address);
                        foreach (EmailAddress member in exp)
                        {
                            Names.Add(new NameEmail() { Email = member.Address, Name = member.Name});
                        }
                    }
                    else
                    {
                        clnemail = contact[0].Mailbox.Address;
                        Names.Add(new NameEmail() { Email = clnemail, Name = contact[0].Contact.DisplayName });
                        if (!NameEmailCache.ContainsKey(clnemail))
                        {
                            lock (_lock)
                            {
                                NameEmailCache.Add(new NameEmail() { Email = clnemail, Name = contact[0].Contact.DisplayName });

                            }
                        }
                        if (String.Compare(clnemail, email, true) != 0 && !NameEmailCache.ContainsKey(email))
                        {
                            lock (_lock)
                            {
                                NameEmailCache.Add(email, new NameEmail() { Email = clnemail, Name = contact[0].Contact.DisplayName });
                            }
                        }
                    }
                }
                else
                {

                    var t = GetBestName(contact, email);
                    Names.Add(t);
                    lock (_lock)
                    {
                        NameEmailCache.Add(t);
                    }
                }
                logger.Log(LogLevel.Debug, "End Resolving Name - GetUser avaliablity");
            }
            return Names;
        }

        public IEnumerable<NameEmail> ResolveNames(IEnumerable<string> queries)
        {
             List<NameEmail> Names = new List<NameEmail>(queries.Count());
            var service = ExchangeConnectorImplementation.GetExchangeService();
            foreach (var email in queries)
            {
                if (NameEmailCache.ContainsKey(email.ToLower()))
                {
                    Names.Add(NameEmailCache[email.ToLower()]);
                    logger.Trace("Cachehit {0}", email);
                    continue;
                }

                //logger.Log(LogLevel.Debug, "Resolving Name - GetUser availiablity");
                
                var contacts = service.ResolveName(email, ResolveNameSearchLocation.DirectoryOnly, true);
                bool? isAccurate=null;
                if (contacts.Count == 1)
                    isAccurate = true;
                foreach (var v in contacts)
                {
                    
                    Names.Add(new NameEmail()
                    {
                        Name=v.Contact.DisplayName,
                        Email=v.Mailbox.Address,
                        isGroup=isGroup(v.Mailbox.MailboxType),
                        isCertain=isAccurate,
                        term=email
                        
                    });
                }
            }
            return Names;
        }
        private Boolean? isGroup(MailboxType? mailboxtype)
        {
            if (!mailboxtype.HasValue)
                return null;
            if (mailboxtype.Value == MailboxType.Contact || mailboxtype.Value == MailboxType.OneOff || mailboxtype.Value == MailboxType.Mailbox)
                return false;
            return true;
        }
        public IEnumerable<NameEmail> GetRooms(IEnumerable<string> queries)
        {
            List<NameEmail> Names = new List<NameEmail>(queries.Count());
            var service = ExchangeConnectorImplementation.GetExchangeService();
            var contacts = service.GetRoomLists();
            foreach (var email in queries)
            {
                if (NameEmailCache.ContainsKey(email.ToLower()))
                {
                    Names.Add(NameEmailCache[email.ToLower()]);
                    logger.Trace("Cachehit {0}", email);
                    continue;
                }

                //logger.Log(LogLevel.Debug, "Resolving Name - GetUser availiablity");
                
                foreach (var v in contacts)
                {
                    if (v.Address == email)
                    {
                        Names.Add(new NameEmail
                        {
                            Name=v.Name,Email=v.Address
                        });
                    }
                }
            }
            return Names;
        }
        public Boolean CreateAppointment(String subject, String body, string emailaddress, DateTime startTime, DateTime endTime)
        {
            var service = ExchangeConnectorImplementation.GetExchangeService();
            Appointment appointment = new Appointment(service);
            appointment.Subject = subject;
            appointment.Body = body;
            appointment.Start = startTime;
            appointment.End = endTime;
            appointment.RequiredAttendees.Add(emailaddress);
            appointment.LegacyFreeBusyStatus = LegacyFreeBusyStatus.Free;
            appointment.Save(SendInvitationsMode.SendOnlyToAll);
            return true;
        }
        public NameEmail GetBestName(NameResolutionCollection coll, String email)
        {

            var n = coll.Select(c => c.Mailbox.Address).Distinct().Count();
            if (n == 1)
            {
                foreach (var nr in coll)
                {
                    if (!String.IsNullOrEmpty(nr.Mailbox.Name))
                        return new NameEmail() { Name = nr.Mailbox.Name, Email = nr.Mailbox.Address };
                }
            }
            return new NameEmail() { Name = "Unknown", Email = email };
        }
        public string testMail(string emailaddress)
        {
            try
            {
                // Connect to Exchange Web Services as user1 at contoso.com.

                var service = ExchangeConnectorImplementation.GetExchangeService();
                // Create the e-mail message, set its properties, and send it to user2@contoso.com, saving a copy to the Sent Items folder. 
                EmailMessage message = new EmailMessage(service);
                message.Subject = "Interesting";
                message.Body = "The proposition has been considered.";
                message.ToRecipients.Add("mv@syscomworld.com");
                message.SendAndSaveCopy();

                return "Message Sent";
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
            }
        }
        public string CleanEmailAddress(string email)
        {
            var ind = email.IndexOf(':');
            if (ind > -1)
            {
                return email.Substring(ind + 1);
            }
            return email;
        }
        private string GetSmtpAddress(Contact c)
        {
            return useFixedSMTPkey ? GetFixedSMTPAddress(c) : GetUnfixedSMTPAdress(c);
        }
        private string GetFixedSMTPAddress(Contact c)
        {
            if (!isEmailAddressKeySet)
            {
                SetSmtpKey(c);
            }
            return c.EmailAddresses[smtpKey].Address;
        }
        private string GetUnfixedSMTPAdress(Contact c)
        {
            for (var i = 0; i < 3; i++)
            {

                var s = c.EmailAddresses[smtpKeys[i]].Address;
                if (String.Compare("SMTP", s.Substring(0, s.IndexOf(':')), true) == 0)
                {
                    return s;
                }
            }
            return "No Valid STMP-Address";
        }
        private void SetSmtpKey(Contact sample)
        {
            lock (_emailkeylock)
            {
                for (var i = 0; i < 3 && !isEmailAddressKeySet; i++)
                {

                    var s = sample.EmailAddresses[smtpKeys[i]].Address;
                    if (String.Compare("SMTP", s.Substring(0, s.IndexOf(':')), true) == 0)
                    {
                        isEmailAddressKeySet = true;
                        smtpKey = smtpKeys[i];
                        logger.Log(LogLevel.Info, "SMTP key is {0}", i);
                    }
                }
            }
        }
        private void TraceContact(NameResolutionCollection c)
        {
            logger.Trace("Returned {0} elements", c.Count);
            foreach (var contact in c)
            {
                logger.Trace(NameResolutonToString(contact));
            }
        }
        private string NameResolutonToString(NameResolution c)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("DisplayName: {0}\n", c.Contact.DisplayName);
            if (c.Contact.EmailAddresses.Contains(EmailAddressKey.EmailAddress1))
                sb.AppendFormat("EmailAddress1:{0}\n", c.Contact.EmailAddresses[EmailAddressKey.EmailAddress1].Address);
            if (c.Contact.EmailAddresses.Contains(EmailAddressKey.EmailAddress2))
                sb.AppendFormat("EmailAddress1:{0}\n", c.Contact.EmailAddresses[EmailAddressKey.EmailAddress2].Address);
            if (c.Contact.EmailAddresses.Contains(EmailAddressKey.EmailAddress3))
                sb.AppendFormat("EmailAddress1:{0}\n", c.Contact.EmailAddresses[EmailAddressKey.EmailAddress3].Address);

            return sb.ToString();
        }
        private static bool ShowEvent(LegacyFreeBusyStatus freeBusy)
        {
            foreach (var b in hideStatuses)
            {
                if (b == freeBusy)
                    return false;
            }
            return true;
        }
    }
}
