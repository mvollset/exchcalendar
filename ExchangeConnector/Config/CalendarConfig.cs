﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Microsoft.Exchange.WebServices.Data;
namespace ExchangeConnector.Config
{
    public class CalendarConfig
    {
        private static ExchangeVersion ver;
        private static bool hasReadExchangeVersion=false;
        private static int[] _daysclosed = null;
        private static object _lock = new object();
        public static string URL { get { return ConfigurationManager.AppSettings["url"]; } }
        public static string User { get { return ConfigurationManager.AppSettings["User"]; } }
        public static string Domain { get { return ConfigurationManager.AppSettings["Domain"]; } }
        public static string Pwd  {get { return ConfigurationManager.AppSettings["Pwd"]; } }
        public static string DateFormat { get { return ConfigurationManager.AppSettings["DateFormat"]; } }
        private static bool? _CanCreateAppointment;
        private static bool? _PerformanceTrace;
        private static bool? _UseNetWorkCredentials;
        private static LegacyFreeBusyStatus[] _hideStatuses;
        private static LegacyFreeBusyStatus[] _busyStatuses;
        public static bool FixedSMTPIndex 
        {
            get
            {
                var p = ConfigurationManager.AppSettings["FixedSMTPIndex"];
                return String.Compare("true", p, true) == 0 ? true : false;    
            }
        }
        public static int[] Daysclosed
        {
            get
            {
                if (_daysclosed == null)
                {
                    var s = ConfigurationManager.AppSettings["Daysclosed"];
                    lock (_lock)
                    {
                        if (String.IsNullOrEmpty(s))
                        {
                            _daysclosed = new int[] { 0, 6 };
                        }
                        else
                        {
                            var sdays = s.Split(',');

                            _daysclosed = new int[sdays.Length];
                            for (var i = 0; i < sdays.Length;i++ )
                            {
                                _daysclosed[i] = Int32.Parse(sdays[i]);
                            }
                        }
                    }
                }
                return _daysclosed;
            }
        }
        public static int Interval
        {
            get
            {
                var s=ConfigurationManager.AppSettings["Interval"];
                return Int16.Parse(s);
            }
        }
        public static int DayStart
        {
            get
            {
                var s = ConfigurationManager.AppSettings["DayStart"];
                return Int16.Parse(s);
            }
        }
        public static int DayEnd
        {
            get
            {
                var s = ConfigurationManager.AppSettings["DayEnd"];
                return Int16.Parse(s);
            }
        }
        public static ExchangeVersion WsVersion
        {
            get
            {
                if (!hasReadExchangeVersion)
                {
                    lock (_lock)
                    {
                        ver=decodeVersion( ConfigurationManager.AppSettings["wsversion"]);
                        hasReadExchangeVersion = true;
                    }
                }
                return ver;
            }
        }
        public static bool CanCreateAppointment
        {
            get
            {
                if (!_CanCreateAppointment.HasValue)
                {
                    var d = ConfigurationManager.AppSettings["CanCreateAppointment"];
                    if (String.IsNullOrEmpty(d)||d.CompareTo("true") != 0)
                        _CanCreateAppointment= false;
                    else
                        _CanCreateAppointment= true;
                }
                    
                return _CanCreateAppointment.Value;
            }
        }
        public static bool PerformanceTrace
        {
            get
            {
                if (!_PerformanceTrace.HasValue)
                {
                    var d = ConfigurationManager.AppSettings["PerformanceTrace"];
                    if (String.IsNullOrEmpty(d) || d.CompareTo("true") != 0)
                        _PerformanceTrace = false;
                    else
                        _PerformanceTrace = true;
                }

                return _PerformanceTrace.Value;
            }
        }
        public static bool UseNetWorkCredentials
        {
            get
            {
                if (!_UseNetWorkCredentials.HasValue)
                {
                    var d = ConfigurationManager.AppSettings["UseNetWorkCredentials"];
                    if (String.IsNullOrEmpty(d) || d.CompareTo("true") != 0)
                        _UseNetWorkCredentials = false;
                    else
                        _UseNetWorkCredentials = true;
                }

                return _UseNetWorkCredentials.Value;
            }
        }
        private static ExchangeVersion decodeVersion(string setting)
        {
            if (String.Compare(setting, "2010") == 0)
                return ExchangeVersion.Exchange2010;
            else if (String.Compare(setting, "2010_SP1") == 0)
                return ExchangeVersion.Exchange2010_SP1;
            else if (String.Compare(setting, "2010_SP2") == 0)
                return ExchangeVersion.Exchange2010_SP2;
            else if (String.Compare(setting, "2013") == 0)
                return ExchangeVersion.Exchange2013;
            return ExchangeVersion.Exchange2007_SP1;
        }
        public  static LegacyFreeBusyStatus[] hideStatuses
        {
            get
            {
                if (_hideStatuses == null)
                {
                    var d = ConfigurationManager.AppSettings["hideStatuses"];
                    _hideStatuses = decodeFreeBusyStatuses(d);
                }
                return _hideStatuses;
            }
        }
        public static LegacyFreeBusyStatus[] busyStatuses
        {
            get
            {
                if (_busyStatuses == null)
                {
                     var d = ConfigurationManager.AppSettings["busyStatuses"];
                     _busyStatuses = decodeFreeBusyStatuses(d);
                }
                return _busyStatuses;
            }
        }
        private static LegacyFreeBusyStatus[] decodeFreeBusyStatuses(string setting)
        {
            if (String.IsNullOrEmpty(setting))
                return new LegacyFreeBusyStatus[0];
            var ss = setting.Split(';');
            var ret = new LegacyFreeBusyStatus[ss.Length];
            var i=0;
            foreach (var s in ss)
            {
                if (s.Equals("Busy"))
                    ret[i++] = LegacyFreeBusyStatus.Busy;
                else if(s.Equals("OOF"))
                    ret[i++] = LegacyFreeBusyStatus.OOF;
                else if (s.Equals("Free"))
                    ret[i++] = LegacyFreeBusyStatus.Free;
                else if (s.Equals("Tentative"))
                    ret[i++] = LegacyFreeBusyStatus.Tentative;
                else if (s.Equals("NoData"))
                    ret[i++] = LegacyFreeBusyStatus.NoData;
            }
            return ret;
        }
        
    }
}
