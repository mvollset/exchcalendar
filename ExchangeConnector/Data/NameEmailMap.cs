﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExchangeConnector.Data
{
    class NameEmailMap:IDictionary<String,NameEmail>
    {
        private Dictionary<String, NameEmail> mailaddressDictionary=new Dictionary<string,NameEmail>();
        private Dictionary<String, NameEmail> nameDictionary = new Dictionary<string, NameEmail>();
        public void Add(NameEmail value)
        {
            if (!mailaddressDictionary.ContainsKey(value.Email.ToLower()))
                mailaddressDictionary.Add(value.Email.ToLower(), value);
            if (!nameDictionary.ContainsKey(value.Name.ToLower()))
                nameDictionary.Add(value.Name.ToLower(), value);
        }
        public void Add(string key,NameEmail value)
        {
            var lowkey = key.ToLower();
            if (!mailaddressDictionary.ContainsKey(lowkey))
                mailaddressDictionary.Add(lowkey, value);
            if (!nameDictionary.ContainsKey(value.Name.ToLower()))
                nameDictionary.Add(value.Name.ToLower(), value);
        }
        public bool ContainsKey(string key)
        {
            var lowkey = key.ToLower();
            var retval = false;
            retval = mailaddressDictionary.ContainsKey(lowkey);
            return retval?retval:nameDictionary.ContainsKey(lowkey);

        }

        public ICollection<string> Keys
        {
            get 
            {
                ICollection<string> s = mailaddressDictionary.Keys;
                ICollection<string> s2 = nameDictionary.Keys;
                return (ICollection<string>) s.Concat(s2);
            }
        }

        public bool Remove(string key)
        {
            if (mailaddressDictionary.ContainsKey(key))
            {
                var e = mailaddressDictionary[key];
                mailaddressDictionary.Remove(key);
                nameDictionary.Remove(e.Name);
                return true;
            }
            if (nameDictionary.ContainsKey(key))
            {
                var e = nameDictionary[key];
                nameDictionary.Remove(key);
                mailaddressDictionary.Remove(e.Email);
                return true;
            }
            return false;
        }

        public bool TryGetValue(string key, out NameEmail value)
        {
            value=null;
            var lowkey = key.ToLower();
            var retval = mailaddressDictionary.ContainsKey(lowkey);
            if (retval)
            {
                value = mailaddressDictionary[lowkey];
                return retval;
            }
            retval = nameDictionary.ContainsKey(lowkey);
            if (retval)
                value = nameDictionary[lowkey];
            return retval;
        }

        public ICollection<NameEmail> Values
        {
            get { return mailaddressDictionary.Values; }
        }

        public NameEmail this[string key]
        {
            get
            {
                var lowkey = key.ToLower();
                return mailaddressDictionary.ContainsKey(key) ? mailaddressDictionary[lowkey] :
                    (nameDictionary.ContainsKey(key) ? nameDictionary[lowkey] : null);
            }
            set
            {
                Remove(key);
            }
        }

        public void Add(KeyValuePair<string, NameEmail> item)
        {
            Add(item.Value);
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(KeyValuePair<string, NameEmail> item)
        {
            return mailaddressDictionary.Contains(item) ? true : nameDictionary.Contains(item);
        }

        public void CopyTo(KeyValuePair<string, NameEmail>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { return mailaddressDictionary.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(KeyValuePair<string, NameEmail> item)
        {

            return Remove(item.Key);
        }

        public IEnumerator<KeyValuePair<string, NameEmail>> GetEnumerator()
        {
            return mailaddressDictionary.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return mailaddressDictionary.GetEnumerator();
        }
    }
}
