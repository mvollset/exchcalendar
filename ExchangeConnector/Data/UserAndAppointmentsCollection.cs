﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExchangeConnector.Data
{
    public class UserAndAppointmentsCollection
    {
        List<UserAndAppointments> collection;
        private DateTime _maxDate = DateTime.MinValue;
        private DateTime _minDate = DateTime.MaxValue;
        private List<String> errorMessages;
        private bool hasErrors;
        private List<UserAndAppointments> appointments;
        private DateTime startTime;
        private DateTime endTime;
        public DateTime maxDate
        {
            get
            {
                return _maxDate;
            }
        }
        public DateTime minDate
        {
            get
            {
                return _minDate;
            }
        }
        public UserAndAppointmentsCollection(List<UserAndAppointments> coll, DateTime startDate, DateTime endDate)
        {
            AddCollection(coll);
            _maxDate = endDate;
            _minDate = startDate;
        }

        public UserAndAppointmentsCollection(List<UserAndAppointments> coll, DateTime startDate, DateTime endDate, List<string> errormessages)
        {
            AddCollection(coll);
            _maxDate = endDate;
            _minDate = startDate;
            errorMessages = errormessages;
            if (errormessages != null && errormessages.Count > 0)
                hasErrors = true;
        }
        public int GetNumberOfDays()
        {
            TimeSpan s = new TimeSpan();
            s = (_maxDate.Date) - _minDate.Date;
            return (int)s.TotalDays;
        }
        public IEnumerable<String> GetUsers()
        {
           
            var s = collection.Select(g => g.DisplayName).Distinct<string>();
            return s;
        }
        public void addError(string errormessage)
        {
            hasErrors = true;
            if (errorMessages == null)
                errorMessages = new List<string>();
            errorMessages.Add(errormessage);
        }
        public void AddCollection(List<UserAndAppointments> coll)
        {

            if (collection == null)
                collection = coll;
            else
                collection.AddRange(coll);
        }
        public int ErrorCount
        {
            get
            {
                return hasErrors ? errorMessages.Count : 0;
            }
        }
        public IEnumerable<string> Errormessages
        {
            get
            {
                return errorMessages;
            }
        }
        public IEnumerable<UserAndAppointments> AppointmentList
        {
            get
            {
                return collection;
            }
        }

    }
}
