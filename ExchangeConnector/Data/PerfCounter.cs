﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExchangeConnector.Data
{
    public class PerfCounter
    {
        Dictionary<String, long> measurements = new Dictionary<string, long>();
        public PerfCounter()
        {

        }
        public void AddCounter(String functionname, long duration)
        {
            if (measurements.ContainsKey(functionname))
            {
                measurements[functionname] += duration;
            }
            else
                measurements.Add(functionname, duration);
        }

        public string ToString()
        {
            StringBuilder sb = new StringBuilder();
            long total = 0;
            foreach (var kvp in measurements)
            {
                total += kvp.Value;
                sb.AppendFormat("{0} : {1}", kvp.Key, kvp.Value);
            }
            sb.AppendFormat("Total : {0}", total);
            return sb.ToString();
        }

    }
}
