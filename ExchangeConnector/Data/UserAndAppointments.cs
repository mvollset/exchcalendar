﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExchangeConnector.Data
{
    public class UserAndAppointments : IEnumerable<MyAppointment>
    {
        public string Email;
        public string DisplayName;
        private List<MyAppointment> appointments;
        public List<MyAppointment> alldaycandidates;
        public DateTime minDate = DateTime.MaxValue;
        public DateTime maxDate = DateTime.MinValue;
        public bool error;
        public String errmsg;
        public UserAndAppointments(bool error, String msg)
        {
            this.error = error;
            errmsg = msg;
        }
        public UserAndAppointments()
        {
          
        }
        public List<MyAppointment> GetAppointmentsForMinutes(int Minutes, DateTime date)
        {
            if (appointments == null)
                return null;
            DateTime m = date.Date.AddMinutes(Minutes);
            return GetAppointmentsForMinutes(m,0);
        }
        public List<MyAppointment> GetAllDay(DateTime date)
        {
            var retval=new List<MyAppointment>();
            if (alldaycandidates == null)
                return retval;
            foreach (MyAppointment app in alldaycandidates)
            {
                if (app.start <= date && app.end > date)
                    retval.Add(app);
            }
            return retval;
        }
        public List<MyAppointment> GetAppointmentsForMinutes(DateTime date,int duration)
        {
            if (appointments == null)
                return null;
            var items = from it in appointments
                        where
                        (it.start <= date && it.end > date||
                        it.start>=date&&it.start<date.AddMinutes(duration)||
                        it.end>date&&it.end<date.AddMinutes(duration)
                        )
                        select it;
            var retval = items.ToList<MyAppointment>();
            return retval;
        }
        public static string AppointmentListToHtml(List<MyAppointment> appointments, DateTime date)
        {
            var apps = appointments.Where(c => c.isDisplayed == false).ToList();
            if (apps.Count == 0)
                return "Opptatt";
            if (apps.Count == 1)
                return String.Format("{0} - {1}", apps[0].Description, FromTo(apps[0], date));
            StringBuilder sb = new StringBuilder("<ul>");
            foreach (var app in apps)
            {
                sb.AppendFormat("<li>{0} - {1}</li>", app.Description, FromTo(app, date));

            }
            sb.AppendFormat("</ul>");
            return sb.ToString();
        }
        public static string FromTo(MyAppointment app, DateTime date)
        {
            String start;
            bool isAllDay;
            String end;
            if (app.start.Date < date.Date)
                start = "00:00";
            else
                start = app.start.ToShortTimeString();
            if (app.end.Date > date)
                end = "00:00";
            else
                end = app.end.ToShortTimeString();
            if (end == "00:00" && start == "00:00")
                return "All day";
            return String.Format("{0}-{1}", start, end);
        }
        public void addAppointment(MyAppointment app)
        {
            if (appointments == null)
                appointments = new List<MyAppointment>();
            appointments.Add(app);
            if (app.end.CompareTo(maxDate) > 0)
                maxDate = app.end;
            if (app.start.CompareTo(minDate) < 0)
                minDate = app.end;

        }
        public void addInfo(MyAppointment app)
        {
            if (alldaycandidates == null)
                alldaycandidates = new List<MyAppointment>();
            alldaycandidates.Add(app);
            if (app.end.CompareTo(maxDate) > 0)
                maxDate = app.end;
            if (app.start.CompareTo(minDate) < 0)
                minDate = app.end;

        }



        public IEnumerator<MyAppointment> GetEnumerator()
        {
            return appointments!=null?appointments.GetEnumerator():new List<MyAppointment>().GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return appointments.GetEnumerator();
        }
    }
}
