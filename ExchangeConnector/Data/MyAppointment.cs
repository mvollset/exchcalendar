﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Exchange.WebServices.Data;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using NLog;
namespace ExchangeConnector.Data
{
    public class MyAppointment
    {
        public DateTime start;
        public DateTime end;
        public int starthour;
        public int endhour;
        public String status;
        public string Description;
        private string _displayClass;
        public bool isDisplayed = false;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public int Duration
        {
            get
            {
                var ts = end - start;
                return ts.TotalMinutes > 480 ? 480 : (int)ts.TotalMinutes;
            }
        }
        public int MinutesOfDay(DateTime dt)
        {
            TimeSpan ts;
            if (start >= dt)
                ts = end - start;
            else
                ts = end - dt;
            return ts.TotalMinutes > 480 ? 480 : (int)ts.TotalMinutes;
        }
        private string CalenderEventToString(CalendarEvent e)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Start: {0}",e.StartTime==null?"<NULL>":e.StartTime.ToString());
            sb.AppendFormat("End: {0}",e.StartTime==null?"<NULL>":e.StartTime.ToString());
            sb.AppendFormat("Details.Subject: {0}",e.Details==null?"N/A":e.Details.Subject);
            sb.AppendFormat("Status:{0}",e.FreeBusyStatus);
            return sb.ToString();
        }
        public MyAppointment(CalendarEvent e)
        {

            if (logger.IsDebugEnabled)
                logger.Debug(CalenderEventToString(e));

            start = e.StartTime;
            starthour = start.Hour;
            end = e.EndTime;
            endhour = end.Hour;
            status = (e.FreeBusyStatus == LegacyFreeBusyStatus.Busy || e.FreeBusyStatus == LegacyFreeBusyStatus.OOF) ? "Busy" : "Tentative";
            Description = e.Details!=null?e.Details.Subject:"Beskrivelse ikke tilgjengelig";
            switch (e.FreeBusyStatus)
            {
                case LegacyFreeBusyStatus.Free:
                    _displayClass = "freeallday";
                    break;
                case LegacyFreeBusyStatus.NoData:
                    _displayClass = "unkallday";
                    break;
                default: 
                _displayClass="busyallday";
                break;
            }
        }
        public string displayClass
        {
            get
            {
                return _displayClass;
            }
        }

    }
}
