﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExchangeConnector.Config;
namespace ExchangeConnector.Data
{
    public class BusyFreeMap
    {
        bool[] busyFreeMinutes=new bool[24*60];
        DateTime date;
        public BusyFreeMap(int startTime, int endTime,DateTime d)
        {
            date = d.Date;

            
            if(IsWorkDay(date.DayOfWeek))
            {
                int i = 0;
                for (i = 0; i < startTime; i++)
                    busyFreeMinutes[i] = false;
                for (i = startTime; i < endTime; i++)
                    busyFreeMinutes[i] = true;
                for (i = endTime; i < 24 * 60; i++)
                    busyFreeMinutes[i] = false;
            }
        }
        public int GetFreeTime()
        {
            var minutes=0;
            for (var i = 0; i < 24 * 60; i++)
            {
                if (busyFreeMinutes[i])
                    minutes++;
            }
            return minutes;
        }
        public void AddTime(DateTime start, DateTime end)
        {
            if (start.Date > date||end < date)
                return;
            DateTime rstart=(start.Date < date)?date:start;
            DateTime rend = end.Date > date ? date.AddDays(1) : end;
            var k = offset(rstart);
            var o = offset(rend);
            for (var i =k ; i <o; i++)
                busyFreeMinutes[i] = false;
        }
        private int offset(DateTime d)
        {
            return (int)(d - date).TotalMinutes;
        }
        public bool IsWorkDay(DayOfWeek d)
        {
            var closeddays = CalendarConfig.Daysclosed;
            var iday = (int)d;
            for (var i = 0; i < closeddays.Length; i++)
            {
                if (closeddays[i] == iday)
                    return false;
            }
            return true;
        }
    }
}
