﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExchangeConnector.Data;
namespace ExchangeConnector
{
    public interface ICalendarConnector
    {
       UserAndAppointmentsCollection GetAppointmentsInPeriod(IEnumerable<string> emailaddresses, DateTime startTime, DateTime endTime);
       Boolean CreateAppointment(String subject, String body, string emailaddress, DateTime startTime, DateTime endTime);
       string testMail(string emailaddress);
       IEnumerable<NameEmail> ResolveNames(IEnumerable<string> queries);
       IEnumerable<NameEmail> GetRooms(IEnumerable<string> queries);
    }
}
