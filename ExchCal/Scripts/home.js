﻿/* Norwegian initialisation for the jQuery UI date picker plugin. */
/* Written by Naimdjon Takhirov (naimdjon@gmail.com). */
var _interval=-1;
jQuery(function ($) {
    $.datepicker.regional['no'] = {
        closeText: 'Lukk',
        prevText: '&laquo;Forrige',
        nextText: 'Neste&raquo;',
        currentText: 'I dag',
        monthNames: ['Januar', 'Februar', 'Mars', 'April', 'Mai', 'Juni',
        'Juli', 'August', 'September', 'Oktober', 'November', 'Desember'],
        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun',
        'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Des'],
        dayNamesShort: ['Søn', 'Man', 'Tir', 'Ons', 'Tor', 'Fre', 'Lør'],
        dayNames: ['Søndag', 'Mandag', 'Tirsdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lørdag'],
        dayNamesMin: ['Sø', 'Ma', 'Ti', 'On', 'To', 'Fr', 'Lø'],
        weekHeader: 'Uke',
        dateFormat: 'dd/mm/yy',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['no']);
});
var prettydateFormat = "D dd. M";
$(document).ready(function () {
    $("#datefrom").datepicker({
        defaultDate: startdate,
        changeMonth: true,
        numberOfMonths: 2,
        showOn: "button",
        dateFormat: "\@",
        buttonImage: _app_path + "Content/calendar.png",
        buttonImageOnly: true,
        onSelect: function (selectedDate) {
            var option = this.id == "from" ? "minDate" : "maxDate",
					instance = $(this).data("datepicker"),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings);
            setformattedDate("#spdatefrom", date);

            //setDateTo(addDays(date, 21));
            //changeDate();

        }
    });

    $("#dateto").datepicker({
        defaultDate: enddate,
        changeMonth: true,
        dateFormat: "@",
        numberOfMonths: 2,
        showOn: "button",
        buttonImage: _app_path +  "Content/calendar.png",
        buttonImageOnly: true,
        onSelect: function (selectedDate) {
            var option = this.id == "from" ? "minDate" : "maxDate",
					instance = $(this).data("datepicker"),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings);
            setformattedDate("#spdateto", date);
            //setDateFrom(addDays(date, -21));
            //changeDate();

        }
    });
    setformattedDate("#spdatefrom", startdate);
    setformattedDate("#spdateto", enddate);
    enableQTip();
});
function enableQTip(delay) {
    if (delay) {
        _interval = window.setInterval(enableQTip, delay);
        return;
    }
    if (_interval > -1) {
        window.clearInterval(_interval);
        _interval = -1;
    }
    $('#freebusy div[qtip]').each(function () {
        $(this).qtip({
            
            content: $(this).attr('qtip'),
            show: 'mouseenter',
            hide: 'mouseleave',
            position: {
            at: "bottom right",
            my: "top right"
                },
        style: {
            tip: true,
            classes: "qtip-cream"
        }
        });
    });

            /*content:, // Use the tooltip attribute of the element for the content,
            position: {
                corner: { target: 'bottomRight', tooltip: 'topRight' }
            },
            style: {
                border: {
                    width: 5,
                    radius: 10
                },
                padding: 10,
                textAlign: 'center',
                tip: true, // Give it a speech bubble tip with automatic corner detection
                name: 'cream' // Style it according to the preset 'cream' style
            }}*/

    $('#freebusy a[qtip]').each(function () {

        $(this).qtip({
            
            content: $(this).attr('qtip'),
            show:'click',
            position: {
                at: "bottom right",
                my: "top right"
            },
            style: {
                tip: true,
                classes: "qtip-cream"
            }
        }
            /*{
            content: $(this).attr('qtip'), // Use the tooltip attribute of the element for the content,
            position: {
                corner: { target: 'bottomRight', tooltip: 'topRight' }
            },
            style: {
                border: {
                    width: 5,
                    radius: 10
                },
                padding: 10,
                textAlign: 'center',
                tip: true, // Give it a speech bubble tip with automatic corner detection
                name: 'green' // Style it according to the preset 'green' style
            }
        }*/);
    });
}
function moveDays(n) {
    var fdate = $("#datefrom").datepicker("getDate");
    fdate=adjustDays(fdate,n);
    setDate("#datefrom",fdate) ;
    setformattedDate("#spdatefrom", fdate);
    fdate = $("#dateto").datepicker("getDate");
    fdate = adjustDays(fdate, n);
    setDate("#dateto", fdate);
    setformattedDate("#spdateto", fdate);
    ajaxPost();
}
function adjustDays(ddate,n) {
    var tz = ddate.getTimezoneOffset();
    ddate = new Date(ddate.getTime() + n * 24 * 3600 * 1000);
    var tz2 = ddate.getTimezoneOffset();
    if (tz != tz2)
        ddate = new Date(ddate.getTime() + 60 * 1000 * -(tz - tz2));
    return ddate;
}
function setDate(datepickerid, date) {
    instance = $(datepickerid).datepicker("setDate", date);
}
function setformattedDate(selector, date, dformat) {
    $(selector).html($.datepicker.formatDate(dformat ? dformat : prettydateFormat, date));
}
function createAppointment(email, dt, hour) {

    $.ajax({
        url: _app_path + 'Home/CreateAppointment',
        type: "POST",
        data: {
            subject: 'subject',
            body: 'body',
            email: email,
            datefrom: dt + hour * 3600 * 1000,
            dateto: dt + (hour + 1) * 3600 * 1000
        },
        success: function (json) {
            window.alert(json.message);
        }
    });


}
function lookup() {
    validate();
    $('#action').val('lookup');
    ajaxPost();
}
function capacity() {
    validate();
    $('#action').val('capacity');
    ajaxPost();
    //document.forms[0].submit();
}
function validate() {
    var datefrom = $('#datefrom').val();
    var dateto = $('#dateto').val();
    if (dateto < datefrom)
        $('#dateto').val(datefrom);

}
function ajaxPost() {
    var pstr = $("form").serializeArray();
    $('#result').load(_app_path + "Home/PartialResult", pstr, function (responseText, textStatus, req)
    {
        if (textStatus == "error") {
            alert(responseText);
        }
        else
            enableQTip(1000);
    }
    );
    
}
function onEmailFocus() {
    $('#divinput').addClass("active");
}
function onEmailLooseFocus() {
    $('#divinput').removeClass("active");
}

