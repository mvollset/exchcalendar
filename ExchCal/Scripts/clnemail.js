﻿function cleanEmail(instring) {
    var res = instring.match(/(<|\()([A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})(>|\))/gi);
    var retval = new Array();
    for (var i = 0; i < res.length; i++) {
        var m = res[i].substr(1,res[i].length-2);
        retval.push(m);
    }
    return retval;
}
function clean(eid) {
    var s = document.getElementById(eid).value;
    document.getElementById('result').value = cleanEmail(s).join(',');
}