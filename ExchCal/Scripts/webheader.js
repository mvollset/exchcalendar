﻿selectedMailAddresses = {};
var paste = false;
var pasteObject = {};
var singleTemplate;
$(document).ready(function () {
    var source = $("#single-template").html();
    var newtr = $('<div class="inputholder" id="inputholder"><span id="selemail"></span><input type="text" id="emailinp" class="email" /><div id="spinner">&nbsp;</div></div>');


    singleTemplate = Handlebars.compile(source);
    $('.email', newtr).autocomplete(
        {
            delay: 500,
            source: _app_path + 'Address/Autocomplete',
            minLength: 4,
            select: function (event, ui) {
                append(ui.item);
                $(this).autocomplete("close");
                $(this).val('');
                $(this).focus();
                return false;
            },
            search: function (event, ui) {
                if (paste) {
                    var inp = $('#emailinp').val();
                    $('#emailinp').autocomplete("disable");
                    $('#spinner').show();
                    var sep = (inp.indexOf(',') > 0) ? ',' : ';';
                    var inparray = inp.split(sep);
                    pasteObject.inparray = inparray;
                    $.getJSON('/Address/Autocomplete', { term: inp }, function (data, stat) {
                        pasteComplete(data);
                    });
                    return false;
                }
                else {
                    //Special handling on ; will automatically validate or add raw email addresses
                    var raw = $('#emailinp').val();
                    if (raw.charAt(raw.length - 1) == ';') {

                        var em = raw.substr(0, raw.length - 1);
                        if (isValidEmailAddress(em)) {
                            //Dont bother to check if this is an email.
                            append({ label: em, value: em });
                            $('#emailinp').val('');
                            $('#emailinp').focus();
                            return false;
                        }
                        else {

                            $.getJSON(_app_path + 'Address/Autocomplete', { term: em }, function (data, stat) {
                                if (data.length && data.length == 1) {
                                    append(data[0]);
                                    $('#emailinp').val('');
                                    $('#emailinp').focus();
                                }
                            });
                            return false;
                        }
                    }
                }
            }
        }
        );
    $("#WIN_0_536870917").append(newtr);
    $('#emailinp').keydown(function (event) {
        if (event.which == 13) {
            event.preventDefault();
        }
        if (event.which == 8) {
            if ($('#emailinp').val() == '') {
                removeLastEmail();
                event.preventDefault();
            }
        }
    });
    $('#emailinp').bind('paste', null, function (e) {
        var element = this;
        paste = true;
        //Settimeout with 0 moves the function to the bottom of the stack
        //will be executed after the Text has been pasted in.....
        setTimeout(function () {
            var text = $(element).val();
            $(element).val(cleanEmail(text));
        }, 0);
        $(element).autocomplete("search");
    });
    var selemail = $('#arid_WIN_0_536870915').val();
    if (selemail != "") {
        $('#emailinp').val(selemail);
        paste = true;
        $('#emailinp').autocomplete("search");
        $('#arid_WIN_0_536870915').val('');
    }
});
function pasteComplete(data) {
    var inparray = pasteObject.inparray;
    var resArray = new Array();
    for (var i = 0; i < inparray.length; i++) {
        var v = checkPaste(inparray[i], data);
        if (v)
            append(v);
        else
            resArray.push(inparray[i]);
    }
    paste = false;
    $('#emailinp').val(resArray.join(';'));
    $('#emailinp').autocomplete("enable");
    $('#spinner').hide();
}
function checkPaste(email, data) {
    for (var i = 0; i < data.length; i++) {
        if (email == data[i].term && data[i].accurate)
            return data[i];
    }
    return null;
}
function append(item) {
    $('#selemail').append(singleTemplate(item));
    var elem = $('#selemail div:last-child')
    appendEmail(item.value);
    item.elem = elem;
    selectedMailAddresses[item.value] = item;
    return false;
}
function removeLastEmail() {
    var ems = new Array();
    for (var s in selectedMailAddresses) {
        if (selectedMailAddresses[s])
            ems.push(s);
    }
    var lastm = ems[ems.length - 1];
    selectedMailAddresses[lastm.email] = null;
    remove(lastm);
    $('#emailinp').focus();
}
function appendEmail(email) {
    var cur = $('#arid_WIN_0_536870915').val();
    if (cur && cur != "")
        $('#arid_WIN_0_536870915').val(cur + ';' + email)
    else
        $('#arid_WIN_0_536870915').val(email);


    function remove(email) {
        var s = selectedMailAddresses[email];
        if (s)
            selectedMailAddresses[email] = null;
        s.elem.remove();
    }
    function mysubmit() {
        var ems = new Array();
        if ($('#emailinp').val() != "") {
            ems.push$(('#emailinp').val());
        }
        for (var s in selectedMailAddresses) {
            if (selectedMailAddresses[s])
                ems.push(s);
        }
        $('#arid_WIN_0_536870915').val(ems.join(';'));
        document.forms[0].submit();
    }
}