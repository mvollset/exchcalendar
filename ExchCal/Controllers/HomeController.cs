﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using ExchCal.Models;
using ExchCal.Helpers;

namespace ExchCal.Controllers
{
    public class HomeController : Controller
    {
        private ActionResult Result(string email, long? datefrom, long? dateto, string myaction, string viewname)
        {
            DateTime startDate;
            DateTime endDate;
            if (Request.Cookies.AllKeys.Contains("inlinedescriptions"))
            {
                ViewData["inlinedescriptions"] = (Request.Cookies["inlinedescriptions"].Value.CompareTo("true") == 0);

            }
            else
            {
                Response.Cookies.Add(new HttpCookie("inlinedescriptions", "true") { Expires = DateTime.Now.AddDays(7) });
                ViewData["inlinedescriptions"] = true;
            }
            if (datefrom.HasValue)
            {
                ViewData["startdate"] = datefrom.Value;
                startDate = DateHelpers.DateTimeFromJSUTC(datefrom.Value);
            }
            else
            {
                startDate = DateTime.Now.Date;
                ViewData["startdate"] = DateHelpers.MilliSecondsSince(startDate);
            }
            if (dateto.HasValue)
            {
                ViewData["enddate"] = dateto.Value;
                endDate = DateHelpers.DateTimeFromJSUTC(dateto.Value);
            }
            else
            {
                endDate = DateTime.Today.AddDays(7);
                ViewData["enddate"] = DateHelpers.MilliSecondsSince(endDate);
            }
            if (!String.IsNullOrWhiteSpace(email))
            {
                var m = new ExchangeModel();
                var ss = email.Split(';').Where(g => (!String.IsNullOrEmpty(g)));
                var s = m.GetAppointmentsInPeriod(ss, startDate.ToLocalTime(), endDate.ToLocalTime().AddDays(1));
                ViewData["email"] = email;
                if (!String.IsNullOrEmpty(myaction) && myaction.CompareTo("capacity") == 0)
                {
                    var capacity = new CapacityPlan(s);
                    DateTime dispstartdate, dispendate;
                    var ts = DateHelpers.GetCompleteWeeks(s.minDate, s.maxDate, DayOfWeek.Monday, out dispstartdate, out dispendate);
                    ViewData["dispstartdate"] = dispstartdate;
                    ViewData["dispenddate"] = dispendate;
                    capacity.Calculate();
                    var a = capacity.GetFreeTime();
                    ViewData["plan"] = capacity.GetFreeTimeTotalUser();
                    ViewData["detailedplan"] = capacity.GetFreeTime();
                }
                else
                    ViewData["busyfree"] = s;
            }

            else
                ViewData["Message"] = "Kalenderoppslag";
            if (String.IsNullOrEmpty(viewname))
                return 1 == 1 ? View("Index2") : View();
            return PartialView(viewname);
            
        }
        public ActionResult Index(string email, long? datefrom, long? dateto, string myaction)
        {

            return Result(email, datefrom, dateto, myaction, null);
            
        }
        public ActionResult CreateAppointment(String subject, String body, String email, long datefrom, long dateto)
        {
            var m = new ExchangeModel();
            m.CreateAppointment(subject, body, email, DateHelpers.DateTimeFromJSUTC(datefrom), DateHelpers.DateTimeFromJSUTC(dateto));
            return Json(new
            {
                success = true,
                message = "Created appointment"
            });

        }
        public ActionResult PartialResult(string email, long? datefrom, long? dateto, string myaction)
        {
     
            return Result(email,datefrom,dateto,myaction, "ResultOnly");
        }
        public ActionResult About()
        {
            return View();
        }
    }
}
