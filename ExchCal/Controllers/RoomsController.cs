﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExchCal.Models;

namespace ExchCal.Controllers
{
    public class RoomsController : Controller
    {
        //
        // GET: /Rooms/

        public ActionResult Index(string email)
        {
            if (String.IsNullOrEmpty(email))
                return View();
            var m = new ExchangeModel();
            var ss = email.Split(';').Where(g => (!String.IsNullOrEmpty(g)));
            var o = m.FindRooms(ss);
            return View(o);
        }

    }
}
