﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExchCal.Models;

namespace ExchCal.Controllers
{
    public class AddressController : Controller
    {
        //
        // GET: /Address/

        public ActionResult Index(string email)
        {
           
            if(String.IsNullOrEmpty(email))
                return View();
            var model = new ExchangeModel();
            var splitEmailaddresses = email.Split(';').Where(g => (!String.IsNullOrEmpty(g)));
            var o = model.ResolveNames(splitEmailaddresses);
            if (!String.IsNullOrWhiteSpace(email))
            {
                ViewData["email"] = email;
            }
                
            return View(o);
        }
        [OutputCache(Duration = 3600, VaryByParam = "term")]
        public ActionResult AutoComplete(string term)
        {
            var model = new ExchangeModel();
            var nameemailset = model.ResolveNames(term.Split(new char[]{',',';'}));
            return Json(
                from nameemail in nameemailset
                select new
                {
                    label = nameemail.Name,
                    value = nameemail.Email,
                    grp = nameemail.isGroup,
                    accurate = nameemail.isCertain,
                    term = nameemail.term
                }
                , JsonRequestBehavior.AllowGet);
        }
        
    }
}
