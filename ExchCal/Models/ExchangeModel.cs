﻿
using System;
using Microsoft.Exchange.WebServices.Data;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using ExchangeConnector;
using ExchangeConnector.Data;
using ExchangeConnector.Config;
namespace ExchCal.Models
{
    public class ExchangeModel
    {
       
      
        public UserAndAppointmentsCollection GetAppointmentsInPeriod(IEnumerable<string> emailaddresses, DateTime startTime, DateTime endTime)
        {
            ICalendarConnector exchange = new ExchangeConnectorImplementation();
            return exchange.GetAppointmentsInPeriod(emailaddresses, startTime, endTime);

        }

        public Boolean CreateAppointment(String subject, String body, string emailaddress, DateTime startTime, DateTime endTime)
        {
            ICalendarConnector exchange = new ExchangeConnectorImplementation();
            return exchange.CreateAppointment(subject, body, emailaddress, startTime, endTime);
        }
        public IEnumerable<NameEmail> ResolveNames(IEnumerable<string> queries)
        {
            ICalendarConnector exchange = new ExchangeConnectorImplementation();
            return exchange.ResolveNames(queries);
        }
        public IEnumerable<NameEmail> FindRooms(IEnumerable<string> queries)
        {
            ICalendarConnector exchange = new ExchangeConnectorImplementation();
            return exchange.GetRooms(queries);
        }
        
    }
   
   
   
    
    public class FreeTime
    {
        public DateTime day;
        public int minutes;
        public string user;
    }
   
}