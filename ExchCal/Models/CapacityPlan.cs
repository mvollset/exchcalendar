﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExchangeConnector.Data;
using ExchangeConnector.Config;
namespace ExchCal.Models
{
    public class CapacityPlan
    {
        private UserAndAppointmentsCollection Collection;
        Dictionary<string, Dictionary<DateTime, int>> result = null;
        List<FreeTime> freetime = null;
        public CapacityPlan(UserAndAppointmentsCollection c)
        {
            Collection = c;
        }
        public void Calculate()
        {
            //Get min date and max date;
            var mindate = Collection.minDate;
            var maxdate = Collection.maxDate;
            var ndays = (int)(maxdate - mindate).TotalDays;
            //Assuming a days work is 480 minutes - 8 Hours
            result = new Dictionary<string, Dictionary<DateTime, int>>();
            foreach (UserAndAppointments userapp in Collection.AppointmentList)
            {
                string user = userapp.DisplayName;
                var uarray = new int[ndays];
                Dictionary<DateTime, int> freet = new Dictionary<DateTime, int>(ndays);
                for (var i = 0; i < ndays; i++)
                {
                    DateTime d = mindate.AddDays(i);
                    var b = new BusyFreeMap(CalendarConfig.DayStart, CalendarConfig.DayEnd, d);

                    foreach (var ap in userapp)
                    {
                        b.AddTime(ap.start, ap.end);
                    }
                    freet.Add(d, b.GetFreeTime());

                }
                result.Add(user, freet);
            }

        }
        private void GenerateFreeTime()
        {
            freetime = new List<FreeTime>();
            foreach (var u in result.Keys)
            {
                foreach (var d in result[u].Keys)
                {
                    freetime.Add(new FreeTime
                    {
                        day = d,
                        user = u,
                        minutes = result[u][d]

                    });
                }
            }
        }
        public IEnumerable<FreeTime> GetFreeTimeAll()
        {
            if (freetime == null)
                GenerateFreeTime();
            var v = from f in freetime
                    group f by f.day into g
                    select new FreeTime { user = "all", day = g.Key, minutes = g.Sum(f => f.minutes) };
            return v;
        }
        public IEnumerable<FreeTime> GetFreeTime()
        {
            if (freetime == null)
                GenerateFreeTime();

            return freetime;
        }
        public IEnumerable<FreeTime> GetFreeTimeTotalUser()
        {
            if (freetime == null)
                GenerateFreeTime();
            var v = from f in freetime
                    group f by f.user into g
                    select new FreeTime { user = g.Key, day = DateTime.MinValue, minutes = g.Sum(f => f.minutes) };
            return v;
        }
        public bool IsWorkDay(DayOfWeek d)
        {
            var closeddays = CalendarConfig.Daysclosed;
            var iday=(int)d;
            for (var i = 0; i < closeddays.Length; i++)
            {
                if (closeddays[i] == i)
                    return false;
            }
            return true;
        }
        private Dictionary<DateTime, int> freeTime(DateTime startdate, int[] uarray)
        {
            var result = new Dictionary<DateTime, int>(uarray.Length);
            for (var i = 0; i < uarray.Length; i++)
            {
                result.Add(startdate.AddDays(i), uarray[i]);
            }
            return result;
        }

    }
}