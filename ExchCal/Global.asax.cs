﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Common;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ExchCal
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            /*routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );*/

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
        public void RegisterMyBundles()
        {

            Bundle bundle = new Bundle("~/Scripts/my", new JsMinify());
            bool searchSubdirectories = false;
            bool throwIfNotExist = false;
            bundle.AddDirectory("~/Scripts", "jquery-2.1.1.min.js", searchSubdirectories, throwIfNotExist);
            bundle.AddDirectory("~/Scripts", "jquery-ui.min-1.11.1.js", searchSubdirectories, throwIfNotExist);
            bool flag5 = false;
            bool flag6 = false;
            bundle.AddDirectory("~/Scripts", "jquery-ui*", flag5, flag6);
            bool flag7 = false;
            bool flag8 = false;
            bundle.AddDirectory("~/Scripts", "jquery.cookie*", flag7, flag8);

            bundle.AddDirectory("~/Scripts", "handlebars.min.js", flag7, flag8);
            bundle.AddDirectory("~/Scripts", "jquery.qtip.min.js", flag7, flag8);
            /*bool flag9 = false;
            bool flag10 = false;
            bundle.AddDirectory("~/Scripts", "jquery.validate*", flag9, flag10);
            bool flag11 = false;
            bundle.AddFile("~/Scripts/MicrosoftAjax.js", flag11);
            bool flag12 = false;
            bundle.AddFile("~/Scripts/MicrosoftMvc.js", flag12);
            bool flag13 = false;
            bool flag14 = false;
            bundle.AddDirectory("~/Scripts", "modernizr*", flag13, flag14);
            bool flag15 = false;
            bundle.AddFile("~/Scripts/AjaxLogin.js", flag15);*/
             BundleTable.Bundles.Add(bundle);
            Bundle bundle2 = new Bundle("~/Content/css", new CssMinify());
            bool flag16 = false;
            bundle2.AddFile("~/Content/site.css", flag16);
            bundle2.AddFile("~/Content/syscom.css", flag16);
            bundle2.AddFile("~/Content/jquery.qtip.min.css", flag16);
            bool flag17 = false;
            bool flag18 = false;
            bundle2.AddDirectory("~/Content/", "jquery.mobile*", flag17, flag18);
            BundleTable.Bundles.Add(bundle2);
            Bundle bundle3 = new Bundle("~/Content/themes/base/css", new CssMinify());
            bool flag19 = false;
            bundle3.AddFile("~/Content/themes/base/jquery.ui.core.css", flag19);
            bool flag20 = false;
            bundle3.AddFile("~/Content/themes/base/jquery.ui.resizable.css", flag20);
            bool flag21 = false;
            bundle3.AddFile("~/Content/themes/base/jquery.ui.selectable.css", flag21);
            bool flag22 = false;
            bundle3.AddFile("~/Content/themes/base/jquery.ui.accordion.css", flag22);
            bool flag23 = false;
            bundle3.AddFile("~/Content/themes/base/jquery.ui.autocomplete.css", flag23);
            bool flag24 = false;
            bundle3.AddFile("~/Content/themes/base/jquery.ui.button.css", flag24);
            bool flag25 = false;
            bundle3.AddFile("~/Content/themes/base/jquery.ui.dialog.css", flag25);
            bool flag26 = false;
            bundle3.AddFile("~/Content/themes/base/jquery.ui.slider.css", flag26);
            bool flag27 = false;
            bundle3.AddFile("~/Content/themes/base/jquery.ui.tabs.css", flag27);
            bool flag28 = false;
            bundle3.AddFile("~/Content/themes/base/jquery.ui.datepicker.css", flag28);
            bool flag29 = false;
            bundle3.AddFile("~/Content/themes/base/jquery.ui.progressbar.css", flag29);
            bool flag30 = false;
            bundle3.AddFile("~/Content/themes/base/jquery.ui.theme.css", flag30);
             BundleTable.Bundles.Add(bundle3);
        
        }
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            // Use LocalDB for Entity Framework by default
            Database.DefaultConnectionFactory = new SqlConnectionFactory("Data Source=(localdb)\v11.0; Integrated Security=True; MultipleActiveResultSets=True");

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            RegisterMyBundles();
            //BundleTable.Bundles.RegisterTemplateBundles();
        }
    }
}