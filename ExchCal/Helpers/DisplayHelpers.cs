﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Expressions;
using ExchCal.Models;
using ExchangeConnector.Data;
using ExchangeConnector.Config;
using System.Text;
namespace ExchCal.Helpers
{
    public static class DisplayHelpers
    {
        public static MvcHtmlString RenderFreeBusy(UserAndAppointmentsCollection coll,bool horizontalUsers = false,bool inlinedescription = true)
        {


            return horizontalUsers ? RenderFreeBusyByUser(coll, inlinedescription) : RenderFreeBusyByPeriod(coll, inlinedescription);

        }
        public static MvcHtmlString RenderFreeBusyByUser(UserAndAppointmentsCollection coll,bool inlinedescription)
        {
                var d = new StringBuilder();           
                d.Append(GetHeaderRow(coll.GetUsers()));
                var slots = GetTimeSlots(coll.minDate, coll.maxDate);
                var prev = DateTime.MinValue;
                foreach (var date in slots)
                {
                    if ((date - prev).Days > 0)
                    {
                        d.Append(GetAllDayRow(date, coll));
                        prev = date;
                    }
                    d.Append(GetAppointmentRow(date, coll, inlinedescription));
                }

            return new MvcHtmlString(d.ToString());

        }

        private static HtmlString GetAllDayRow(DateTime date, UserAndAppointmentsCollection coll)
        {
            StringBuilder res = new StringBuilder(String.Format("<tr><td></td>"));
            foreach (UserAndAppointments item in coll.AppointmentList)
            {
                res.Append("<td>");
                foreach (var c in item.GetAllDay(date))
                {
                    
                    res.AppendFormat ("<div class=\"{0}\">{1}</div>", c.displayClass, c.Description);
                    c.isDisplayed = true;
                }
                res.Append("</td>");
            }

            return new MvcHtmlString(res.ToString());
        }
        public static MvcHtmlString RenderFreeBusyByPeriod(UserAndAppointmentsCollection coll, bool inlinedescription)
        {
            var d = new StringBuilder();
            d.Append(GetHeaderRow(coll.minDate, coll.GetNumberOfDays()));
            foreach (var item in coll.AppointmentList)
            {
                d.Append(GetAppointmentRow(item, coll.minDate, coll.GetNumberOfDays(), inlinedescription));
            }

            return new MvcHtmlString(d.ToString());

        }
        public static MvcHtmlString RenderPlan(IEnumerable<FreeTime> freetime)
        {
            var d = new StringBuilder("<ul>");
            foreach (var f in freetime)
            {
                d.AppendFormat("<li>Total: {0} - {1} hours free</li>", f.day.ToShortDateString(), (int)f.minutes/60); 
            }
            return new MvcHtmlString(d.Append("</ul>").ToString());
        }
        public static MvcHtmlString RenderPlanUser(IEnumerable<FreeTime> freetime,string v)
        {
            var d = new StringBuilder("<ul>");
            foreach (var f in freetime)
            {
                d.AppendFormat("<li>Total: {0} - {1} hours free</li>", f.user, (int)f.minutes / 60);
            }
            return new MvcHtmlString(d.Append("</ul>").ToString());
        }
        public static MvcHtmlString RenderPlanByDay(IEnumerable<FreeTime> freetime,DateTime date)
        {
            var d = new StringBuilder("<ul>");
            foreach (var f in freetime.Where(g=>g.day.Date==date.Date))
            {
                d.AppendFormat("<li>{0} - {1} free</li>", f.user,  (int)f.minutes / 60);
            }
            return new MvcHtmlString(d.Append("</ul>").ToString());
        }
        private static HtmlString GetHeaderRow(DateTime startDate, int numDays)
        {
            StringBuilder headerrow = new StringBuilder("<tr>");
            var interval = GetIntervalMin();
            var colspan = (int)60 / interval;
            
            for (var i = 0; i < numDays; i++)
            {
                var day = startDate.DayOfWeek;
                headerrow.AppendFormat("<th class=\"newday\" rowspan=\"{0}\">{1}</th>",DisplayMinuteRow()?2:1, i == 0 ? startDate.ToShortDateString() : startDate.ToString("dd.MM"));
                for (var k = GetStartWorkHours(day); k < GetEndWorkHours(day); k+=60)
                {
                    headerrow.AppendFormat("<th colspan=\"{0}\">{1}</th>", colspan,FormatTime(k,true));
                }
                
                startDate = startDate.AddDays(1);
            }
            headerrow.Append("</tr>");
            if (DisplayMinuteRow())
                headerrow.Append(GetHeaderRow2(startDate, numDays));
            return new HtmlString(headerrow.ToString());
        }
        private static HtmlString GetHeaderRow(IEnumerable<string> emailadresses)
        {
            StringBuilder headerrow = new StringBuilder("<tr><th></th>");
            
            foreach (var adress in emailadresses)
            {
                headerrow.AppendFormat("<th class=\"newday\">{0}</th>",adress);
            }
            headerrow.Append("</tr>");
            return new HtmlString(headerrow.ToString());
        }
        private static String GetHeaderRow2(DateTime startDate, int numDays)
        {
            StringBuilder headerrow = new StringBuilder("<tr>");
            var interval = GetIntervalMin();
            for (var i = 0; i < numDays; i++)
            {
                var day = startDate.DayOfWeek;
                //headerrow.AppendFormat("<th class=\"newday\">&nbsp;</th>");
                for (var k = GetStartWorkHours(day); k < GetEndWorkHours(day); k += GetIntervalMin())
                {
                    headerrow.AppendFormat("<th>{0}</th>", FormatTime(k,false));
                }

                startDate = startDate.AddDays(1);
            }
            headerrow.Append("</tr>");

            return headerrow.ToString();
        }
        private static HtmlString GetAppointmentRow(DateTime date,UserAndAppointmentsCollection apps,bool inlinedescription)
        {
            double passed = (DateTime.Now-date).TotalMinutes;
            String classString="";
            if (passed > 0)
            {
                classString = String.Format("class=\"{0}\"", passed < GetIntervalMin() ? "now passed" : "passed");
            }
            StringBuilder res = new StringBuilder(String.Format("<tr {0}>",classString));
            //var slots = GetTimeSlots(startdate, enddate);
            res.AppendFormat("<td>{0}</td>", date.ToString(DateFormat()));
            var mssince = DateHelpers.MilliSecondsSince(date);
            foreach (var app in apps.AppointmentList)
            {
                if (app.error)
                {
                     res.Append(new HtmlString("<td>##Feil##</td>"));
                }
                else
                {
                    string status;
                    if(CalendarConfig.CanCreateAppointment)
                     {
                        status = String.Format("<a href=\"#\" onclick=\"createAppointment('{0}',{1},{2});\" qtip=\"Opprett avtale\">Ledig</a>", 
                        app.Email, mssince, GetIntervalMin());
                        }
                    else
                    {
                        status="<div>Ledig</div>";
                    }
                    var c = app.GetAppointmentsForMinutes(date, GetIntervalMin());
                    if (c != null && c.Count != 0)
                    {
                        status = String.Format(inlinedescription ? "<div>{0}</div>" : "<div  qtip=\"{0}\">---</div>", new HtmlString(UserAndAppointments.AppointmentListToHtml(c, date)), c.Count);

                    }
                    HtmlString htmlstatus = new HtmlString("<td>" + status + "</td>");
                    res.Append(htmlstatus);
                }
            }
            return new HtmlString(res.ToString());

        }
        private static HtmlString GetAppointmentRow(UserAndAppointments app, DateTime startDate, int numDays, bool inlinedescription)
        {
            StringBuilder res = new StringBuilder("<tr>");
            var d2 = startDate.Date;
            if (app.error)
            {

                for (var k = 0; k < numDays; k++)
                {
                    res.AppendFormat("<td>{0}</td>", k == 0 ? app.errmsg : "");

                    DayOfWeek d = startDate.DayOfWeek;
                    for (var i = GetStartWorkHours(d); i < GetEndWorkHours(d); i += GetIntervalMin())
                    {
                        var nd2 = d2.AddMinutes(i);
                      
                        HtmlString htmlstatus = new HtmlString("<td>N/A</td>");
                        res.Append(htmlstatus);
                    }
                    d2 = d2.AddDays(1);
                }

            }
            else
            {
                for (var k = 0; k < numDays; k++)
                {
                    res.AppendFormat("<td>{0}</td>", k == 0 ? app.Email : "");

                    DayOfWeek d = startDate.DayOfWeek;
                    for (var i = GetStartWorkHours(d); i <GetEndWorkHours(d); i += GetIntervalMin())
                    {
                        var nd2 = d2.AddMinutes(i);
                        var status = String.Format("<a href=\"#\" onclick=\"createAppointment('{0}',{1},{2});\" qtip=\"Opprett avtale\">Free</a>", app.Email, ExchCal.Helpers.DateHelpers.MilliSecondsSince(nd2), i);
                        var c = app.GetAppointmentsForMinutes(i, nd2);
                        if (c != null && c.Count != 0)
                        {
                            status = String.Format(inlinedescription?"<div>{0}</div>":"<div  qtip=\"{0}\">Busy</div>", new HtmlString(UserAndAppointments.AppointmentListToHtml(c, d2)), c.Count);

                        }
                        HtmlString htmlstatus = new HtmlString("<td>" + status + "</td>");
                        res.Append(htmlstatus);
                    }
                    d2 = d2.AddDays(1);
                }
            }
            res.Append("</tr>");
            return new HtmlString(res.ToString());
        }
        private static IEnumerable<DateTime> GetTimeSlots(DateTime start, DateTime end)
        {
            var sd = start.Date.AddMinutes(GetStartWorkHours(start.DayOfWeek));
            var ed = end.Date;
            var res = new List<DateTime>();
            var interval = GetIntervalMin();
            var cinterval=0;
            var duration=GetEndWorkHours(sd.DayOfWeek)-GetStartWorkHours(sd.DayOfWeek);
            while (sd < ed)
            {
                if (cinterval < duration)
                {

                    res.Add(sd.AddMinutes(cinterval));
                    cinterval += interval;
                }
                else
                {
                    sd = sd.Date.AddDays(1);
                    sd=sd.AddMinutes(GetStartWorkHours(sd.DayOfWeek));
                    cinterval = 0;
                    duration = GetEndWorkHours(sd.DayOfWeek) - GetStartWorkHours(sd.DayOfWeek);

                }

            }
            return res;
        }
       
        static string FormatTime(int MinOffset,bool showhour)
        {
            var d = MinOffset % 60;
            if (showhour)
            {
                
                if (d == 0)
                    return String.Format("{0:00}", MinOffset / 60);
            }
            return String.Format(":{0:00}", d);
        }
        public static int GetStartWorkHours(DayOfWeek day)
        {
            return CalendarConfig.DayStart;
        }
        public static int GetEndWorkHours(DayOfWeek day)
        {
            return CalendarConfig.DayEnd;
        }
        public static int GetIntervalMin()
        {
            return CalendarConfig.Interval;
        }
        public static bool DisplayMinuteRow()
        {
            return !(GetIntervalMin() % 60 == 0);
        }
        public static string DateFormat()
        {
            return CalendarConfig.DateFormat;
        }

    }
}