﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
namespace ExchCal.Helpers
{
    public static class DateHelpers
    {
        private static DateTime dawnofjs = new DateTime(1970, 1, 1);
        private static TimeZone tz = TimeZone.CurrentTimeZone;
        public static DateTime DateTimeFromJSUTC(long ms)
        {

            return dawnofjs.AddMilliseconds(ms);
        }
        public static double MilliSecondsSince(DateTime dt)
        {
            var ts = dt.Subtract(dawnofjs);
            return ts.TotalMilliseconds;
        }
        public static int IsoWeek(DateTime dt)
        {
            GregorianCalendar g = new GregorianCalendar();
            return g.GetWeekOfYear(dt, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }
        public static TimeSpan GetCompleteWeeks(DateTime start, DateTime end,DayOfWeek WeekStart,out DateTime dispstart, out DateTime dispenddate)
        {
            var n =(int) start.Date.DayOfWeek;
            var dn = (int)WeekStart;
            dispstart = start.Date.AddDays(-(n - dn));
            var en=(int)end.Date.DayOfWeek;
            if (en == 0)
                dispenddate = end.Date.AddDays(1);
            else
                dispenddate = end.Date.AddDays(7-(en - dn));
            return (dispstart - dispenddate);

        }
    }
}